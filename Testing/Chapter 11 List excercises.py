# Finds the sum of spots 3 - 16
my_list = [7, 4, 2, 7, 3, 4, 6, 7, 8, 9, 7, 0, 10, 7, 0, 1, 7, 6, 5, 7, 3, 2, 7, 9, 9, 8, 7]

total = 0

for thing in my_list[3:17]:
    total += thing

print("The sum of 3-16 = " + str(total))

# Finds the sum of spots 2 - 9
my_list = [7, 4, 2, 7, 3, 4, 6, 7, 8, 9, 7, 0, 10, 7, 0, 1, 7, 6, 5, 7, 3, 2, 7, 9, 9, 8, 7]

total = 0

for i in range(2, 10):
    total += my_list[i]

print("The sum of 2-9 = " + str(total))

print()

# Displays the # of 0s, 3s, and 7s presents
number_of_zeroes = 0
number_of_threes = 0
number_of_sevens = 0

for thing in my_list:
    if thing == 0:
        number_of_zeroes += 1
    elif thing == 3:
        number_of_threes += 1
    elif thing == 7:
        number_of_sevens += 1

print("# of 0s = " + str(number_of_zeroes))
print("# of 3s = " + str(number_of_threes))
print("# of 7s = " + str(number_of_sevens))

print()

# Creates a new list with all 7s removed and displays it
my_list = [7, 4, 2, 7, 3, 4, 6, 7, 8, 9, 7, 0, 10, 7, 0, 1, 7, 6, 5, 7, 3, 2, 7, 9, 9, 8, 7]

total_number_of_sevens = 0

for thing in my_list:
    if thing == 7:
        my_list.remove(thing)

print("new list with all 7s removed = " + str(my_list))

for thing in my_list:
    if thing == 7:
        total_number_of_sevens += 1

print("# of 7s = " + str(total_number_of_sevens))

# Odds and Evens
my_list1 = [2, 4, 6, 8, 10, 12, 14]

odds1 = []
evens1 = []

for thing in my_list1:
    if thing % 2 == 0:
        evens1.append(thing)
    else:
        odds1.append(thing)

print("\nodds = " + str(odds1))
print("evens = " + str(evens1))

my_list2 = [1, 2, 3, 4, 5, 6, 7, 8, 9]

odds2 = []
evens2 = []

for thing in my_list2:
    if thing % 2 == 0:
        evens2.append(thing)
    else:
        odds2.append(thing)

print("\nodds = " + str(odds2))
print("evens = " + str(evens2))

my_list3 = [10, 20, 21, 23, 24, 40, 55, 60, 61]

odds3 = []
evens3 = []

for thing in my_list3:
    if thing % 2 == 0:
        evens3.append(thing)
    else:
        odds3.append(thing)

print("\nodds = " + str(odds3))
print("evens = " + str(evens3))

