import arcade

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600


def draw_grass():
    # Draw the ground
    arcade.draw_lrtb_rectangle_filled(0, SCREEN_WIDTH, SCREEN_HEIGHT / 3, 0, arcade.color.AIR_SUPERIORITY_BLUE)


def draw_snow_person(x, y):
    # Draw a snow person

    # snow
    arcade.draw_circle_filled(300 + x - 285 - 15, 200 + y - 200 + 60, 60, arcade.color.WHITE)
    arcade.draw_circle_filled(300 + x - 285 - 15, 280 + y - 200 + 60, 50, arcade.color.WHITE)
    arcade.draw_circle_filled(300 + x - 285 - 15, 340 + y - 200 + 60, 40, arcade.color.WHITE)

    # Eyes
    arcade.draw_circle_filled(285 + x - 285 - 15, 350 + y - 200 + 60, 5, arcade.color.BLACK)
    arcade.draw_circle_filled(315 + x - 285 - 15, 350 + y - 200 + 60, 5, arcade.color.BLACK)

    # Draw a point at x, y for reference
    # arcade.draw_point(x, y, arcade.color. RED, 5)


def on_draw(delta_time):
    """ Draw everything"""
    arcade.start_render()
    draw_grass()
    draw_snow_person(on_draw.snow_person1_x, 140)
    draw_snow_person(450, on_draw.snow_person2_y)

# make the snow person move right

    on_draw.snow_person1_x += 1
    on_draw.snow_person1_y += 1
    on_draw.snow_person2_y += on_draw.speed

    on_draw.speed += 0.1

# create a value for our snow person 1x to start at


on_draw.snow_person1_x = 150
on_draw.snow_person1_y = 140
on_draw.snow_person2_y = 180
on_draw.speed = 1


def main():
    arcade.open_window(SCREEN_WIDTH, SCREEN_HEIGHT, "Drawing with Functions")
    arcade.set_background_color(arcade.color.DARK_BLUE)
    arcade.start_render()

    arcade.schedule(on_draw, 1/60)
    
    arcade.run()
    
# call the main function


main()
