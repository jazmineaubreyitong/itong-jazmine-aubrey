# simple class to handle basic collisions

import pygame

class GameObject():
    def __init__(self):
        self.image = pygame.Surface([20, 20])
        self.colour = (255, 0, 0)
        self.image.fill(self.colour)

        self.x = 0
        self.y = 0

        self.velocity_x = 0
        self.velocity_y = 0

        self.alive = True

        self.rect = self.image.get_rect()

    def draw(self, screen):
        screen.blit(self.image, [self.x, self.y])

    def updateRect(self):
        self.rect = pygame.Rect(self.x, self.y, self.image.get_width(), self.image.get_height())

    def update(self):
        self.x += self.velocity_x
        self.y += self.velocity_y

        self.updateRect()