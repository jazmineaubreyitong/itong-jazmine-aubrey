# To print a list

x = [1, 2]

x[1] = 17

# Can't change if brackets are ()
y = (54, 65, 34)

print(x[1])

# example 1
my_list = [101, 20, 10, 50, 60, "Hello"]

for item_variable in my_list:
    print(item_variable)
# Or (thing)

# example 2
for index in range(len(my_list)):
    print(my_list[index])

# example 3
for index, value in enumerate(my_list):
    print(index, value)

# example 4
print(my_list)

my_list.append(42)

print(my_list)

# example 5
my_list = []

for i in range(5):
    user_input = input("Enter a number: ")
    user_input = int(user_input)
    my_list.append(user_input)
    print(my_list)

# example 6
x = "Hello World!"

print(x[3:7])

print()