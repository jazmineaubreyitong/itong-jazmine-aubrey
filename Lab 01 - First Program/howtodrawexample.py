"""
this is a sample program showing how to draw stuff
Jazmine Aubrey Itong
Computer Science 20
"""

import arcade

#open a window
arcade.open_window(1280, 720, "Awesome Picture")

#set_background_color
arcade.set_background_color(arcade.color.ALMOND)

#get ready to draw
arcade.start_render()

#Drawing code will go here
arcade.draw_lrtb_rectangle_filled(15, 60, 150, 100, arcade.color.BLUSH)

arcade.draw_lrtb_rectangle_outline(400, 800, 150, 100, arcade.color.AFRICAN_VIOLET, 20)

arcade.draw_rectangle_filled(900, 360, 100, 50, arcade.color.BLACK)

arcade.draw_rectangle_filled(800, 500, 100, 50, arcade.color.AMETHYST, 45)

arcade.draw_circle_filled(150, 400, 150, arcade.color.AIR_SUPERIORITY_BLUE)

arcade.draw_ellipse_filled(50, 100, 100, 150, arcade.color.AFRICAN_VIOLET)

#finish drawing
arcade.finish_render()

#keep the window running until someone closes it
arcade.run()