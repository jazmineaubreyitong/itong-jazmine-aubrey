"""
Jazmine Aubrey Itong
Tommy Avram
Mr. Birrell
computer science 20
March 27, 2018
Lab 4
"""

def lions_game():
    print("Welcome to the race from Lions! ")
    user_name = input("What is your name? ")
    print()

    # Variables
    import random
    drink = 20
    done = False
    distance = 20
    energy = 47
    lions = 12
    # Introduction
    print("----------------------------------------------------------------------------------")
    print("Welcome, " + user_name)
    print("You are on a safari in Africa and you see some wild gazelles. ")
    print("You have spotted some lions! ")
    print("The lions have spotted you and have started to chase you! ")
    print("You throw some spare meat to give you some time. ")
    print("The meat is buying you a lot of time and you are now 12km ahead of the lions. ")
    print("There is a village 20km away from you that you must run to! ")
    print("You must drink and rest along the journey, if you do not, you will die! ")
    print("The lions have begun to chase you. ")
    print("Your hydration level starts at 20mL. ")
    print("Your energy level starts at 47. ")
    print("----------------------------------------------------------------------------------")
    print()

    # User Options
    while done == False:
        print("a = full speed")
        print("b = moderate speed")
        print("c = status check")
        print("d = drink")
        print("e = rest")
        print("f = quit")

        # User Choice a
        print()
        user_choice = input("What would you choose? ")
        print("----------------------------------------------------------------------------------")

        if user_choice.lower() == "a":
            distance -= 2
            drink -= random.randrange(3, 7)
            energy -= random.randrange(4, 8)
            print("Your distance from the village is " + str(distance) + "km. ")
            print("Your amount of hydration left is " + str(drink) + "mL. ")
            print("Your amount of energy left is  " + str(energy))
            print("The lions are " + str(lions) + "km behind you. ")

        # User Choice b
        elif user_choice.lower() == "b":
            distance -= 1
            drink -= random.randrange(1, 4)
            energy -= 3
            print("Your distance from the village is " + str(distance) + "km. ")
            print("Your amount of water left before you reach dehydration is " + str(drink) + "mL. ")
            print("Your amount of energy left is  " + str(energy))
            print("The lions are " + str(lions) + "km behind you. ")

        # User Choice c
        elif user_choice.lower() == "c":
            print("Your distance from the village is " + str(distance) + "km. ")
            print("Your amount of energy left is  " + str(energy))
            print("Your amount of hydration left is " + str(drink) + "mL. ")
            print("The lions are " + str(lions) + "km behind you. ")

        # User Choice d
        elif user_choice.lower() == "d":
            drink += 15
            energy += 5
            lions -= random.randrange(4, 8)
            print("You took a drink! ")
            print("Your amount of hydration left is " + str(drink) + "mL. ")
            print("The lions are " + str(lions) + "km behind you. ")
            print("Your distance from the village is " + str(distance) + "km. ")

        # User Choice e
        elif user_choice.lower() == "e":
            energy += 3
            print("You took a rest! ")
            print("Your energy left is " + str(energy))
            lions -= random.randrange(4, 8)
            print("tThe lions are " + str(lions) + "km behind you. ")
            print("Your distance from the village is " + str(distance) + "km. ")

        # User Choice f
        elif user_choice.lower() == "f":
            done = True
            print("You quit the game! ")
        else:
            print("Invalid selection, please try again. ")
        print()

        # Random Events
        rocket = random.randrange(0, 101)
        if rocket == 56:
            print("You found a rocket in the desert and you flew to the village!")
            print("You have won the game! ")
            play_again = input("Do you want to play again? ")
            if play_again.lower() == "yes":
                lions_game()
            else:
                done = True
        bike = random.randrange(0, 11)
        if bike == 2:
            print("You found a bike and have travelled 3km before the tire popped. ")
            distance -= 3
            lions += 2

        # End Sequences
        if distance < 1:
            print("Congratulations " + user_name + ", you have made into the village and survived!!! ")
            play_again = input("Do you want to play again? ")
            print()
            if play_again.lower() == "yes":
                lions_game()
            else:
                done = True
        elif lions < 1:
            print(" It's Game over. The lions caught you. ")
            play_again = input("Do you want to play again? ")
            if play_again.lower() == "yes":
                lions_game()
            else:
                done = True
        elif drink < 1:
            print("You died of Dehydration. ")
            print("Game Over! ")
            play_again = input("\nDo you want to play again? ")
            if play_again.lower() == "yes":
                lions_game()
            else:
                done = True
        elif energy < 1:
            print("You died of exhaustion. ")
            print("Game Over! ")
            play_again = input("Do you want to play again? ")
            if play_again.lower() == "yes":
                lions_game()
            else:
                done = True

    # Warnings for Variables
        if done == False:
            if lions < 7 and lions > 0:
                print("WARNING! The lions are getting close! ")
            if drink < 7 and drink > 0:
                print("WARNING! You are getting dehydrated! ")
            if energy < 10 and energy > 0:
                print("WARNING! You are getting exhausted, you need to drink or take a rest. ")


lions_game()