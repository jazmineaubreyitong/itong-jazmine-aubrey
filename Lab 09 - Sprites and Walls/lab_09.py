"""
CS 20
Wendy Chen
Steffi Uyam
Jazmine Aubrey Itong
May 11, 2018
LAB 9 FUNCTION
"""
import random


# Part one
# Find the smallest value
print("# Part One")
print()


def min3(number_1, number_2, number_3):
    if number_1 < number_2 and number_1 < number_3:
        return number_1

    elif number_2 < number_1 and number_2 < number_3:
        return number_2

    elif number_3 < number_1 and number_3 < number_2:
        return number_3

    elif number_1 == number_2 and number_2 == number_3:
        return number_1 or number_2 or number_3


# Test code
print(min3(4, 7, 5))
print(min3(4, 5, 5))
print(min3(4, 4, 4))
print(min3(-2, -6, -100))
print(min3("Z", "B", "A"))

print()


# Part two
# Draw the box
print("# Part Two")
print()


def box(height, width):
    for i in range(height):
        for a in range(width):
            print("*", end=" ")
        print()


# Test Code
box(7, 5)   # Print a box 7 high, 5 across
print()    # Blank line
box(3, 2)   # Print a box 3 high, 2 across
print()    # Blank line
box(3, 10)  # Print a box 3 high, 10 across

print()


# Part 3
# Find the position of the number in the list
# a is my_list
print("# Part Three")
print()


def find(a, key):
    for thing in range(len(a)):
        if a[thing] == key:
            print("Found " + str(key) + " at position " + str(thing))


# Test Code
my_list = [36, 31, 79, 96, 36, 91, 77, 33, 19, 3, 34, 12, 70, 12, 54, 98, 86, 11, 17, 17]
find(my_list, 12)
find(my_list, 91)
find(my_list, 80)

print()


# Part 4
# The first function
print("# Part Four")
print()
my_list = []


def create_list(list_size):
    for i in range(list_size):
        number = random.randrange(1, 7)
        my_list.append(number)

    return my_list


# Test Code
my_list = create_list(5)
print(my_list)

print()


# The second function
# The list
# l is list
def count_list(l, number):
    count_list = 0
    for i in l:
        if i == number:
            count_list += 1

    return count_list


# Test Code
count = count_list([1, 2, 3, 3, 3, 4, 2, 1], 3)
print(count)

print()


# The third function
# Count the average of the list
# l is list
def average_list(l):
    total_of_list = 0
    for i in l:
        total_of_list += i
        avg = total_of_list / len(l)

    return avg


# Test Code
avg = average_list([1, 2, 3])
print(avg)

print()


# Main program
# Print 10000 times
my_list = create_list(10000)
print(my_list)
print()

# Count numbers
# Count the numbers of one
count_1 = count_list(my_list, 1)
print("There are", count_1, "number one.")

# Count the numbers of two
count_2 = count_list(my_list, 2)
print("There are", count_2, "number two.")

# Count the numbers of three
count_3 = count_list(my_list, 3)
print("There are", count_3, "number three.")

# Count the numbers of four
count_4 = count_list(my_list, 4)
print("There are", count_4, "number three.")

# Count the numbers of five
count_5 = count_list(my_list, 5)
print("There are", count_5, "number three.")

# Count the numbers of six
count_6 = count_list(my_list, 6)
print("There are", count_6, "number three.")
print()

# Average of random number
avg = average_list(my_list)
print("The average of this list is", str(avg))
