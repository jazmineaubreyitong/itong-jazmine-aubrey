"""
Jazmine Aubrey Itong
computer science 20
March 9, 2018
Lab 4
"""
# Math Quiz

user_name = input("What is your name? ")

print()

print("Hi, " + user_name + ". Welcome to my Math Quiz!")

print()

# Input

total_score = 0

# First Question

# Type of question: Multiple Choice Answer
print("1. Liza has 13 apples, on the way home she lost 4 apples. How many apples are left?")
print("a. 9")
print("b. 5")
print("c. 7")
question_one = input("? ")

if question_one.lower() == "a":
    print("your answer is correct.")
    total_score += 1
elif question_one.lower() == "b":
    print("sorry, your answer is wrong.")
elif question_one.lower() == "c":
    print("sorry, your answer is wrong.")

print()

# Second Question

# Type of question: Menu Answer/Multiple Choice Answer/Text Answer
question_two = input("2. 5 * 8 = 40. true or false? ")

if question_two.lower() == "true":
    print("Your answer is correct.")
    total_score += 1
elif question_one.lower() == "false":
    print("sorry, your answer is wrong.")

print()

# Third Question

# Type of question: Text Answer
question_three = input("3. what is it called when two angles add up to 180 degrees? ")

if question_three.lower() == "supplementary":
    print("your answer is correct.")
    total_score += 1
else:
    print("sorry, your answer is wrong.")

print()

# Fourth Question

# Type of question: Number Answer
question_four = input("4. (2x + 4)(2y + 6)=? ")

if question_four.lower() == "4(xy + 3x + 2y + 6)":
    print("your answer is correct!")
    total_score += 1
else:
    print("sorry, your answer is wrong.")

print()

# Fifth Question

# Type of question: Bonus Answer -
print("5. Is Mr.Birrel the best teacher ever?")
print("a. yes")
print("b. the best!")
print("c. super!!!")
question_five = input("? ")

if question_five.lower() == "c":
    print("your answer is correct!")
    total_score += 1
elif question_five.lower() == "b":
    print("sorry, your answer is wrong.")
elif question_five.lower() == "a":
    print("sorry, your answer is wrong.")

print()

# Total Calculator
total_percentage = total_score / 5 * 100

# Output
print(user_name + ", Your total score is " + str(total_score))
print("Your total percentage is " + str(total_percentage) + " Thank you for participating! ")
