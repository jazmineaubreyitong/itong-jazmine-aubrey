"""
Jazmine Aubrey Itong
computer science 20
February 27, 2018
Lab 3
"""

import arcade

SCREEN_WIDTH = 1000
SCREEN_HEIGHT = 700


def draw_grass():

    # Draw the ground
    arcade.draw_lrtb_rectangle_filled(0, SCREEN_WIDTH, SCREEN_HEIGHT / 3.8, 0, color=(66, 143, 71))
    arcade.draw_line(50, 60, 50, 70, (0, 0, 0), 2)
    arcade.draw_line(45, 65, 45, 75, (0, 0, 0), 2)
    arcade.draw_line(40, 60, 40, 70, (0, 0, 0), 2)


def draw_sun(x, y):

    # sun
    arcade.draw_circle_filled(650 + x - 650, 500 + y - 480, 60, color=(244, 252, 0))


def draw_clouds(x, y):

    # Draw the clouds
    arcade.draw_ellipse_filled(205 + x - 60, 500 + y - 480, 50, 30, color=(250, 250, 250))
    arcade.draw_ellipse_filled(220 + x - 60, 490 + y - 480, 50, 30, color=(250, 250, 250))
    arcade.draw_ellipse_filled(180 + x - 60, 490 + y - 480, 50, 30, color=(250, 250, 250))
    arcade.draw_ellipse_filled(200 + x - 60, 480 + y - 480, 50, 30, color=(250, 250, 250))

    arcade.draw_ellipse_filled(350 + x - 60, 540 + y - 480, 50, 30, color=(250, 250, 250))
    arcade.draw_ellipse_filled(360 + x - 60, 520 + y - 480, 50, 30, color=(250, 250, 250))
    arcade.draw_ellipse_filled(380 + x - 60, 550 + y - 480, 50, 30, color=(250, 250, 250))
    arcade.draw_ellipse_filled(400 + x - 60, 530 + y - 480, 50, 30, color=(250, 250, 250))

    arcade.draw_ellipse_filled(600 + x - 60, 540 + y - 480, 50, 30, color=(250, 250, 250))
    arcade.draw_ellipse_filled(700 + x - 60, 480 + y - 480, 50, 30, color=(250, 250, 250))



def draw_birds():

    # Draw the birds
    arcade.draw_parabola_outline(500, 520, 510, 6.2, color=(0, 0, 0), border_width=0.5, tilt_angle=300)
    arcade.draw_parabola_outline(515, 520, 525, 6.2, color=(0, 0, 0), border_width=0.5, tilt_angle=60)

    arcade.draw_parabola_outline(530, 500, 540, 6.2, color=(0, 0, 0), border_width=0.5, tilt_angle=300)
    arcade.draw_parabola_outline(545, 500, 555, 6.2, color=(0, 0, 0), border_width=0.5, tilt_angle=55)

    arcade.draw_parabola_outline(470, 500, 480, 6.2, color=(0, 0, 0), border_width=0.5, tilt_angle=300)
    arcade.draw_parabola_outline(485, 500, 495, 6.2, color=(0, 0, 0), border_width=0.5, tilt_angle=60)


def draw_ball(x, y):

    # Draw the ball
    arcade.draw_circle_filled(300 + x - 285 - 15, 200 + y - 200 + 60, 60, color=(219, 152, 35))
    arcade.draw_circle_outline(300 + x - 285 - 15, 200 + y - 200 + 60, 60, color=(0, 0, 0))
    arcade.draw_line(300 + x - 285 - 15, 260 + y - 200 + 60, 300 + x - 285 - 15, 140 + y - 200 + 60, arcade.color.BLACK, 1)
    arcade.draw_line(270 + x - 285 - 15, 250 + y - 200 + 60, 270 + x - 285 - 15, 150 + y - 200 + 60, arcade.color.BLACK, 1)
    arcade.draw_line(330 + x - 285 - 15, 250 + y - 200 + 60, 330 + x - 285 - 15, 150 + y - 200 + 60, arcade.color.BLACK, 1)
    arcade.draw_line(240 + x - 285 - 15, 200 + y - 200 + 60, 360 + x - 285 - 15, 200 + y - 200 + 60, arcade.color.BLACK, 1)

    # Draw a point at x, y for reference

    # arcade.draw_point(x, y, arcade.color.RED, 5)


def on_draw(delta_time):

    """Draw Everything"""
    arcade.start_render()
    draw_sun(650, on_draw.sun_y)
    draw_grass()
    draw_ball(on_draw.ball_x, 140)
    draw_birds()
    draw_clouds(on_draw.clouds_x, 500)

# make the ball move to the right

    on_draw.ball_x += 2
    on_draw.ball_y += 2
    on_draw.ball_y += on_draw.speed

    on_draw.speed += 3

# make the clouds move to the left

    on_draw.clouds_x -= 2
    on_draw.clouds_y -= 2

# make the sun move down

    on_draw.sun_y -= 2

# create a value for the ball 1x to start at

on_draw.ball_x = 150
on_draw.ball_y = 140
on_draw.speed = 1

on_draw.clouds_x = 500
on_draw.clouds_y = 490
on_draw.speed = 1

on_draw.sun_y = 500

def main():

    arcade.open_window(SCREEN_WIDTH, SCREEN_HEIGHT, "Drawing with Functions")
    arcade.set_background_color(color=(152, 203, 235))

    arcade.schedule(on_draw, 1/60)

    arcade.run()

# call the main function


main()
