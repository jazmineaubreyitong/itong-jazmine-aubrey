"""
Jazmine Aubrey Itong
computer science 20
February 13, 2018
Lab 2
"""

import arcade

# open a window
arcade.open_window(1280, 720, "Lab 2 project")

# set_background_color
arcade.set_background_color((178, 222, 237))

# get ready to draw
arcade.start_render()

# drawing code will go here

# sun
arcade.draw_circle_filled(1050, 500, 60, (242, 255, 13))

# clouds
arcade.draw_ellipse_filled(305, 500, 50, 30, (250, 250, 250))
arcade.draw_ellipse_filled(320, 490, 50, 30, (250, 250, 250))
arcade.draw_ellipse_filled(280, 490, 50, 30, (250, 250, 250))
arcade.draw_ellipse_filled(300, 480, 50, 30, (250, 250, 250))

arcade.draw_ellipse_filled(450, 550, 50, 30, (250, 250, 250))
arcade.draw_ellipse_filled(460, 520, 50, 30, (250, 250, 250))
arcade.draw_ellipse_filled(480, 570, 50, 30, (250, 250, 250))
arcade.draw_ellipse_filled(500, 540, 50, 30, (250, 250, 250))

arcade.draw_ellipse_filled(1000, 540, 50, 30, (250, 250, 250))
arcade.draw_ellipse_filled(1100, 480, 50, 30, (250, 250, 250))

# grass
arcade.draw_rectangle_filled(50, 100, 2500, 200, (59, 196, 100))

# water
arcade.draw_ellipse_filled(200, 100, 250, 50, (0, 161, 247))
arcade.draw_ellipse_outline(200, 100, 250, 50, (0, 0, 0))

# house
arcade.draw_rectangle_filled(700, 200, 200, 200, (153, 144, 99))
arcade.draw_rectangle_outline(700, 200, 200, 200, (0, 0, 0))

# roof of the house
arcade.draw_triangle_filled(810, 300, 590, 300, 700, 500, (66, 62, 42))

# window
arcade.draw_rectangle_filled(750, 250, 50, 60, arcade.color.ALMOND)
arcade.draw_line(775, 250, 725, 250, (0, 0, 0))
arcade.draw_rectangle_outline(750, 250, 50, 60, arcade.color.BLACK)
arcade.draw_line(750, 280, 750, 220, (0, 0, 0))

# door
arcade.draw_rectangle_filled(700, 135, 40, 70, arcade.color.ALABAMA_CRIMSON)
arcade.draw_rectangle_outline(700, 135, 40, 70, arcade.color.BLACK)

# door handle
arcade.draw_circle_filled(690, 130, 2, arcade.color.BLACK)

# finish drawing
arcade.finish_render()

# keep the window running until someone closes it
arcade.run()
