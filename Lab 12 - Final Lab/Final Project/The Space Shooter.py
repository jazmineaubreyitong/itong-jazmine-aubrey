"""
Jazmine Aubrey Itong
Mr. Birrell
Computer Science 20
June 2018
"""

# Takes from other files
import pygame
import math
import random

# Variables for colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (48, 150, 52)
RED = (255, 0, 0)
GRAY = (132, 138, 156)
NEON_GREEN = (212, 255, 0)


# Player Class
class Player(pygame.sprite.Sprite):  # Makes player into sprite

    # Creates a variable for speed of player
    change_x = 0
    change_y = 0

    def __init__(self, x, y):  # x and y is the position of the player

        # Call the parent's constructor
        super().__init__()
        self.image = pygame.image.load("Spaceship.png").convert()  # Takes in the image of spaceship in the program
        self.image.set_colorkey(WHITE)  # Removes any white background in the image
        self.rect = self.image.get_rect()  # Converts the image to sprite

        # Takes in the x and y given and sets it as the default spawn location
        self.rect.y = y
        self.rect.x = x

    # x and y determines the speed of the player depending of which keys are pressed
    def changespeed(self, x, y):
        self.change_x += x
        self.change_y += y

    # Allows the player to move and to detect if it touches any walls
    def move(self, walls):

        # Move left/right according to the speed
        self.rect.x += self.change_x

        # Recognizes if player hits the wall on the left and right side
        player_hit_list = pygame.sprite.spritecollide(self, walls, False)

        for player in player_hit_list:

            # If the player is moving right because the speed is positive, change the right side into the left side
            if self.change_x > 0:
                self.rect.right = player.rect.left
            # If the player is moving left because the speed is anything else other than positive,
            # change the left side in the right side
            else:
                self.rect.left = player.rect.right

        # Move up/down according to the speed
        self.rect.y += self.change_y

        # Recognizes if player hits the wall on the left and right side
        player_hit_list = pygame.sprite.spritecollide(self, walls, False)

        for player in player_hit_list:

            # If the player is moving down because the speed is positive, change the base side into the top side
            if self.change_y > 0:
                self.rect.bottom = player.rect.top
            # If the player is moving up because the speed is negative, change the top side into the top side
            else:
                self.rect.top = player.rect.bottom

# Enemy class
class Enemy(pygame.sprite.Sprite):  # Makes enemies into sprites

    def __init__(self):
        # Call the parent constructor
        super().__init__()
        self.image = open_enemy_image  # Sets the default look of the enemy into open mouthed monster
        self.rect = self.image.get_rect()  # Converts image to sprite

    # Allows the enemy to move and animate
    def update(self):

        self.rect.x += -1  # Speed of the enemy

        # if the dice_image equals to 2, then image of the monster will change to create a opening mouth animation
        dice_image = random.randrange(1, 200)
        if dice_image == (2):
            if enemy.image == open_enemy_image:
                enemy.image = closed_enemy_image

            elif enemy.image == closed_enemy_image:
                enemy.image = open_enemy_image


# Star class
class Star(pygame.sprite.Sprite):  # Makes stars into sprites
    def __init__(self):
        # Call the parent constructor
        super().__init__()
        self.image = pygame.Surface([1, 1])  # Creates a small square
        self.image.fill(WHITE)  # that small square is then colored into white
        self.rect = self.image.get_rect()  # that small square is also changed into a sprite

    # Allows the speed of the star
    def update(self):
        self.rect.x += -2


# Wall class
class Wall(pygame.sprite.Sprite):  # Makes walls into sprites
    def __init__(self, x, y, width, height):  # Takes in the position where the wall is gonna be as well as
                                                # the area of the wall
        super().__init__()

        self.image = pygame.Surface([width, height])  # Sets the size of the wall depending of what is given
        self.image.fill(GRAY)  # The color of the wall is filled in Gray
        self.rect = self.image.get_rect()  # Makes the wall into a sprite

        # Sets the position of the wall
        self.rect.y = y
        self.rect.x = x


# Bullet Class
class Bullet(pygame.sprite.Sprite):  # Makes bullets into sprites
    def __init__(self, start_x, start_y, dest_x, dest_y):  # Takes in the starting position of the bullet as well as
                                                            #  the position of the mouse
        # Call the parent constructor
        super().__init__()

        self.image = pygame.Surface([5, 5])  # Makes a square that represents the bullet
        self.image.fill(NEON_GREEN)  # the bullet is then colored in neon green
        self.rect = self.image.get_rect()  # Makes that square a sprite

        # Sets the spawn location of the bullet
        self.rect.x = start_x
        self.rect.y = start_y

        # start_x and y are converted from integers to floats because angles require precise measurements
        self.floating_point_x = start_x
        self.floating_point_y = start_y

        # Creates the angle of the direction of the bullet using the starting coordinates and the mouse coordinates
        x_diff = dest_x - start_x
        y_diff = dest_y - start_y
        angle = math.atan2(y_diff, x_diff)

        # This is the Speed of the bullet
        velocity = 10
        self.change_x = math.cos(angle) * velocity
        self.change_y = math.sin(angle) * velocity

    # Allows the bullet to move
    def update(self):

        # Adds the speed of the bullet into the converted float
        self.floating_point_y += self.change_y
        self.floating_point_x += self.change_x

        # Sets the speed of the bullet
        self.rect.y = int(self.floating_point_y)
        self.rect.x = int(self.floating_point_x)

        # If the bullet flies of the screen, get rid of it.
        if self.rect.x < 0 or self.rect.x > SCREEN_WIDTH or self.rect.y < 0 or self.rect.y > SCREEN_HEIGHT:
            self.kill()


pygame.init()  # Activate pygame program

# Variables
intro_wasd = False  # Locks the enemies from spawning and the ability to shoot until the user press the w,a,s,and d keys
intro_shoot = False  # Locks the enemies from spawning until the player shoots
intro_survive = 0  # Timer for the text survive until it disappears
gameover_key = False  # Locks the ability to appear this certain text until the player dies
player_dead = False  # This will lock the ability to shoot and to move if the player dies
player_score = 0  # Sets the score of the player for every hit to the enemy
spawn_time = 0  # Sets a certain time the enemies will spawn
enemy_count = 5  # Sets the amount of enemies spawning
set_boost = False  # Locks the ability to show the boosters behind the spaceship until the user pressed the "d" button
done = False  # Loops the program until done becomes true
SCREEN_WIDTH = 820  # Sets the width of the screen
SCREEN_HEIGHT = 720  # Sets the height of the screen

# LISTS
player_list = pygame.sprite.Group()
bullet_list = pygame.sprite.Group()
wall_list = pygame.sprite.Group()
enemy_list = pygame.sprite.Group()
bullet_list = pygame.sprite.Group()
star_list = pygame.sprite.Group()
all_sprites_list = pygame.sprite.Group()

screen = pygame.display.set_mode([SCREEN_WIDTH, SCREEN_HEIGHT])  # sets the size of the screen
pygame.display.set_caption("The Space Shooter")  # Title of the game is given
clock = pygame.time.Clock()  # Used to manage how fast the screen updates
pygame.mouse.set_visible(1)  # Does not hide the mouse

# Creates the walls
wall = Wall(0, 0, 1, SCREEN_HEIGHT)  # Left side
wall_list.add(wall)
wall = Wall(0, 0, SCREEN_WIDTH, 1)  # Top side
wall_list.add(wall)
wall = Wall(0, SCREEN_HEIGHT, SCREEN_WIDTH, 1)  # Bottom side
wall_list.add(wall)
wall = Wall(SCREEN_WIDTH, 0, 1, SCREEN_HEIGHT)  # Right side
wall_list.add(wall)
all_sprites_list.add(wall)

# Takes in the image of the enemy
open_enemy_image = pygame.image.load("Open_monster.png").convert()
open_enemy_image.set_colorkey(WHITE)
closed_enemy_image = pygame.image.load("Closed_monster.png").convert()
closed_enemy_image.set_colorkey(WHITE)

new_image = open_enemy_image  # This will determine what the current image of the enemy will be

# Takes in the image of the boosts
boost_image = pygame.image.load("Boost.png").convert()
boost_image.set_colorkey(WHITE)

# Creates Player
player = Player(SCREEN_WIDTH/2 - 50, SCREEN_HEIGHT/2 - 20)
player_list.add(player)
all_sprites_list.add(player)
# Creates enemy
enemy = Enemy()
# Creates star
star = Star()


# -------- Main Program Loop -----------
while not done:
    # --- Event Processing

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

        # Mouse controls
        if event.type == pygame.MOUSEBUTTONDOWN and player_dead == False:
            # Fire a bullet if the user clicks the mouse button
            if intro_wasd == True:

                # Get the mouse position
                pos = pygame.mouse.get_pos()

                mouse_x = pos[0]
                mouse_y = pos[1]

                # Create the bullet based on where we are, and where we want to go.
                bullet = Bullet(player.rect.x + 100, player.rect.y + 70, mouse_x, mouse_y)

                # Add the bullet to the lists
                all_sprites_list.add(bullet)
                bullet_list.add(bullet)

                intro_shoot = True  # Allows the enemies to spawn

        # Keyboard controls
        if event.type == pygame.KEYDOWN and player_dead == False:

            if event.key == pygame.K_a:
                player.changespeed(-1, 0)
                intro_wasd = True  # Allows the shooting ability to work
            if event.key == pygame.K_d:
                player.changespeed(1, 0)
                set_boost = True  # if pressed, boost image appears
                intro_wasd = True  # Allows the shooting ability to work
            if event.key == pygame.K_w:
                player.changespeed(0, -1)
                intro_wasd = True  # Allows the shooting ability to work
            if event.key == pygame.K_s:
                player.changespeed(0, 1)
                intro_wasd = True  # Allows the shooting ability to work

        if event.type == pygame.KEYUP and player_dead == False:

            if event.key == pygame.K_a:
                player.changespeed(1, 0)
            if event.key == pygame.K_d:
                player.changespeed(-1, 0)
                set_boost = False  # disappear boost image
            if event.key == pygame.K_w:
                player.changespeed(0, 1)

            if event.key == pygame.K_s:
                player.changespeed(0, -1)

    # if player is still alive, allow it to move
    for i in player_list:
        if i == player:
            player.move(wall_list)

    # if player scores a certain number, increase spawn of enemies
    if player_score == (10) & enemy_count == (10):
        enemy_count += 5
    elif player_score == (5) & enemy_count == (5):
        enemy_count += 5

    if intro_shoot == True:  # if the ability to shoot is used the first time, then enemies is spawned
        spawn_time += 1  # timer for spawn
        if spawn_time == 1000:  # time to which the enemy will spawn

            # Enemy will spawn
            for i in range(enemy_count):
                enemy = Enemy()
                enemy.rect.x = random.randrange(1000, 3000)
                enemy.rect.y = random.randrange(SCREEN_HEIGHT - 50)
                enemy_list.add(enemy)
                all_sprites_list.add(enemy)

            spawn_time = 0  # timer is reset

    # Spawn stars
    for i in range(1):
        star = Star()
        star.rect.x = random.randrange(1000, 1001)
        star.rect.y = random.randrange(SCREEN_HEIGHT)
        star_list.add(star)
        all_sprites_list.add(star)

    # Remove stars once they go beyond the left side to avoid lag
    for star in star_list:
        if star.rect.x <= -10:
            star_list.remove(star)
            all_sprites_list.remove(star)

    all_sprites_list.update()  # Updates everything sprite within the list

    # Bullet to enemy collision
    for bullet in bullet_list:

        # See if it hit a enemy
        enemy_hit_list = pygame.sprite.spritecollide(bullet, enemy_list, True)

        # For each enemy hit, remove the bullet and add to the score
        for enemy in enemy_hit_list:
            bullet_list.remove(bullet)
            all_sprites_list.remove(bullet)
            player_score += 1
            if player_score >= (50):
                enemy_count += 1

        # Remove the bullet if it flies up off the screen
        if bullet.rect.y < -10:
            bullet_list.remove(bullet)
            all_sprites_list.remove(bullet)

    # Enemy to player collision
    for enemy in enemy_list:

        # See if it hit a player
        player_hit_list = pygame.sprite.spritecollide(player, enemy_list, True)

        # For each player hit, remove the player and activate gameover screen and disallow any controls
        for player in player_hit_list:
            player_list.remove(player)
            all_sprites_list.remove(player)
            gameover_key = True
            player_dead = True

    # If enemy flies off the left side, remove it
    for enemy in enemy_list:
        if enemy.rect.x == -10:
            enemy_list.remove(enemy)
            all_sprites_list.remove(enemy)

    # Scoring
    font_size = 25
    SCORE = 0

    # background color
    screen.fill(BLACK)

    # sets the font
    font = pygame.font.SysFont('Comic Sans MS', 25, True, False)
    survive_font = pygame.font.SysFont('Comic Sans MS', 70, True, False)

    # score text
    score_text = font.render("Score: " + str(player_score), True, WHITE)

    # control text
    controls_w = font.render("[W] - Up", True, WHITE)
    controls_a = font.render("[A] - Left", True, WHITE)
    controls_s = font.render("[S] - Down", True, WHITE)
    controls_d = font.render("[D] - Right", True, WHITE)
    controls_mouse = font.render("[LEFT CLICK] - Shoot", True, WHITE)

    # SURVIVE text
    survive_text = survive_font.render("SURVIVE", True, WHITE)

    # GAMEOVER text
    gameover_text = survive_font.render("GAME OVER", True, WHITE)

    all_sprites_list.draw(screen)  # Draws every sprites

    # if user press "d", show boosters
    if set_boost == True and player_dead == False:
        screen.blit(boost_image, [player.rect.x - 20, player.rect.y])

    # if player is dead, gameover appears
    if gameover_key == True:
        screen.blit(gameover_text, [SCREEN_WIDTH/2 - 150, SCREEN_HEIGHT/2 - 20])

    # if user shoots for the first time, show score and SURVIVE text
    if intro_shoot == True:
        screen.blit(score_text, [50, 50])
        intro_survive += 1

        if intro_survive <= 1000:  # sets the timer for the SURVIVE text until it disappears
            screen.blit(survive_text, [SCREEN_WIDTH/2 - 150, SCREEN_HEIGHT/2 - 20])

    # Shows the mouse controls if player pressed the wasd keys for the first time
    elif intro_wasd == True:
        screen.blit(controls_mouse, [player.rect.x + 100, player.rect.y + 90])
    # Shows the wasd controls when the program is opened
    elif intro_wasd == False:
        screen.blit(controls_w, [350, 250])
        screen.blit(controls_a, [200, 360])
        screen.blit(controls_s, [350, 500])
        screen.blit(controls_d, [510, 360])

    # Update the screen
    pygame.display.flip()

# Limit frames per second
clock.tick(60)

# Close the window and quit.
pygame.quit()
